package vard.verificare;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by XYZ on 7/1/2015.
 */
public class Fisier {

    public void creaza(){
        String adresa = Environment.getExternalStorageDirectory().getAbsolutePath();
        Log.d("Adresa este: ", adresa);
        String nume = adresa + "/android.txt";
        File fisier = new File(nume);
        FileWriter scrie;
        BufferedWriter buf;
        try {
            scrie = new FileWriter(fisier);
            buf = new BufferedWriter(scrie);
            buf.append("Ceva");
            buf.flush();
            buf.close();
        }catch(Exception e){

            e.printStackTrace();
        }
    }
    }
