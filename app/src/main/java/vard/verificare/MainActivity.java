package vard.verificare;

import android.app.Activity;
import android.os.Debug;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.*;
import java.net.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.Buffer;
import java.util.Scanner;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import org.apache.http.impl.conn.LoggingSessionInputBuffer;

public class MainActivity extends Activity {


    private Socket s;
    private PrintWriter outp;
    private BufferedReader inp1;
    private ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);


        }
        final EditText msg = (EditText) findViewById(R.id.etMsg);
        Button send = (Button) findViewById(R.id.bSend);
        final TextView convo = (TextView) findViewById(R.id.tvConvo);
        final TextView status = (TextView) findViewById(R.id.tvStatus);
        final TextView mServer = (TextView) findViewById(R.id.sC);
        final TextView holderView = new TextView(this);
        final Button creaza= (Button) findViewById(R.id.creaza);
        final Button chat= (Button) findViewById(R.id.bChat);
        final Button comunica=(Button) findViewById(R.id.bComunica);
        final ViewFlipper nextPage = (ViewFlipper) findViewById(R.id.nextPage);
        final EditText ipinit= (EditText) findViewById(R.id.tIp);
        final Button logIn= (Button) findViewById(R.id.bLog);
        final TextView cStare = (TextView) findViewById(R.id.vStatus);
        final Button offline= (Button) findViewById(R.id.bOffline);
        final Button inapoi=(Button)  findViewById(R.id.bInapoi);
        final Button inapoi2=(Button)  findViewById(R.id.bInapoi2);
        final Spinner slista= (Spinner) findViewById(R.id.sLista);


        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        slista.setAdapter(adapter);
//        adapter.add("h");
//        adapter.add("2");


        inapoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPage.showPrevious();
                nextPage.showPrevious();
            }
        });
        inapoi2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPage.showPrevious();
            }
        });

        logIn.setOnClickListener(new  View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String ipStore;

                ipStore= ipinit.getText().toString();
                try {
                    s = new Socket(ipStore, 8080);
                    inp1 = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    outp = new PrintWriter(s.getOutputStream(), true);
                    //UPDATE STATUS -> pagina 2
                    cStare.setText("Contectat la server!");
                    nextPage.showNext();

                    slista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            System.out.println(position);
                            adapter.getItem(position);
                            System.out.println(adapter.getItem(position));

                            outp.println("SendFile:"+adapter.getItem(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    comunica.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            nextPage.showNext();
                            nextPage.showNext();
                            outp.println("RequestFile");

                        }
                    });
                }
                catch (Exception e){

                    cStare.setText("Eroare de conectare!");
                            e.printStackTrace();
                }
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nextPage.showNext();
            }
        });

        ServerClient cititor = new ServerClient();
        holderView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                Log.d("TEST1",holderView.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String text = holderView.getText().toString();

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("TEST2", text);
                        mServer.append(text);
                        ia(text);
                    }
                });
//                mServer.setText(holderView.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.d("TEST3",holderView.getText().toString());
            }
        });




        try {
            s = new Socket("10.0.2.2", 8080);
            inp1 = new BufferedReader(new InputStreamReader(s.getInputStream()));

            cititor.init(inp1, holderView);
            cititor.start();

            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = msg.getText().toString();
                    Log.d("TEST", message);
                    status.setText("...");


                    status.setText("Established connection..");


                    if (message != null) {
                        if (msg.getText().toString().trim() == "QUIT") {
                            try {
                                s.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            status.setText("Disconnected from server.");

                        } else {
                            try {

                                convo.append(message + "\n");
                                outp.println(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else
                        status.setText("Problem in connection..!");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        creaza.setOnClickListener(new View.OnClickListener() {
            Fisier f = new Fisier();
            @Override
            public void onClick(View v) {

                f.creaza();

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void ia(String inp) {

//    System.out.println(inp);
        if (inp.contains("Fisier:")) {
            System.out.println("Fara Fisier: " + inp.substring(7));//inp.substring(7);
//            adapter.add(inp.substring(7));
            if (adapter.getCount() == 0)
                adapter.add(inp.substring(7));
int a=0;
            for (int i = 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i).equals(inp.substring(7))) {
                   a=a+1;
                }
            }
            if(a==0)
                adapter.add(inp.substring(7));
        }


    }


}
