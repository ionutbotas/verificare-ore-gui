package vard.verificare.schema;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Liviu on 7/2/2015.
 */
@XStreamAlias("proiect")
public class Proiect {
    @XStreamAlias("denumire")
    @XStreamAsAttribute
    private String denumire;
    @XStreamImplicit
    private List<Zona> zone;

    public Proiect(String denumire){
        this.denumire=denumire;
    }

    public List<Zona> getZone() {
        return zone;
    }

    public void setZone(List<Zona> zone) {
        this.zone = zone;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }
}
