package vard.verificare.schema;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Liviu on 7/2/2015.
 */
@XStreamAlias("zona")
public class Zona {
    @XStreamAlias("denumire")
    @XStreamAsAttribute
    private String denumire;
    @XStreamImplicit
    private List<Activitate> activitati;

    public Zona(String denumire){
        this.denumire = denumire;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public List<Activitate> getActivitati() {
        return activitati;
    }

    public void setActivitati(List<Activitate> activitati) {
        this.activitati = activitati;
    }
}
