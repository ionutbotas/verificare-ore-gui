package vard.verificare.schema;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import java.util.List;

/**
 * Created by Liviu on 7/1/2015.
 */
@XStreamAlias("activitate")
public class Activitate {
    @XStreamAlias("denumire")
    @XStreamAsAttribute
    private String denumire;
    @XStreamAlias("procent")
    @XStreamAsAttribute
    private double procent;

    public Activitate(String denumire, double procent){
        this.denumire=denumire;
        this.procent = procent;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public double getProcent() {
        return procent;
    }

    public void setProcent(double procent) {
        this.procent = procent;
    }
}
